from django.db import models

from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE


class Item(models.Model):
    name = models.CharField(max_length=255, unique=True, null=False)


class Quote(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE

    name = models.CharField(max_length=255, unique=True, null=False)
    price = models.PositiveIntegerField(null=False)
    items = models.ManyToManyField(Item)


class QuoteLog(models.Model):
    quote = models.ForeignKey(Quote, on_delete=models.CASCADE, null=True)
    created_date = models.DateTimeField(auto_now=True)
    OPERATION_CHOICES = ([("C", "CREATE"), ("U", "UPDATE"), ("D", "DELETE")])
    operation = models.CharField(max_length=1, choices=OPERATION_CHOICES)
    error_code = models.IntegerField(null=True)
    message = models.TextField(max_length=1024, null=True)
