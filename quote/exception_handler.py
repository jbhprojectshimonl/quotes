import logging

from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.response import Response
from rest_framework.serializers import ModelSerializer
from rest_framework.views import exception_handler

from quote.constants import GENERAL_ERR__ERR_CODE
from quote.views import CustomException

logger = logging.getLogger("django_logger")


def get_one_err(not_valid_serialize: ModelSerializer) -> ErrorDetail:
    """
    Returns one error (priority to custom error)
    :param not_valid_serialize: serialise object (it's includes errors)
    :return: ErrorDetail object
    """
    errors = getattr(not_valid_serialize, "custom_errors", dict()).values()
    if len(errors) == 0:
        errors = not_valid_serialize.errors.values()
    if len(errors) == 0:
        return ErrorDetail("unknown error")
    err = iter(errors).__next__()
    return err[0]


class ErrorHandler:
    """
    Handler for errors (used by custom_exception_handler)
    Displays error in following format
        {"errorCode": 1, "description": "quote already exist", "level": "error"}
    Returns one error in this format
    Saves one error in this format
    Writes to the log file any of error in the above format
    """

    def __init__(self, not_valid_serialize: ModelSerializer):
        self.not_valid_serialize = not_valid_serialize

    @staticmethod
    def exception_as_dict(exception: ErrorDetail) -> dict:
        """
        Returns specific exception in following format
        {"errorCode": 1, "description": "quote already exist", "level": "error"}
        :param exception: ErrorDetail exception
        :return: Dictionary representing an exception in the above format
        """
        description = str(exception)
        code: str = exception.code
        error_code = code if code.isdigit() else GENERAL_ERR__ERR_CODE
        level = "error"
        return {"errCode": error_code, "description": description, "level": level}

    def response_dict(self) -> dict:
        """
        Returns one error in the above format
        :return: Dictionary
        """
        exception: ErrorDetail = get_one_err(self.not_valid_serialize)
        return self.exception_as_dict(exception)

    def to_log(self) -> None:
        """
        Writes to the log file any of error in the above format
        :return: None
        """
        custom_errors: dict = getattr(self.not_valid_serialize, "custom_errors", dict())
        errors = self.not_valid_serialize.errors
        list_err_val = []
        for err in list(errors.values()):
            list_err_val += err
        for err in list(custom_errors.values()):
            list_err_val += err
        for err in list_err_val:
            logger.error(self.exception_as_dict(err))


def custom_exception_handler(exc: CustomException, context) -> Response:
    """
    Exception handler
    Sends error as a response in the above format
    Writes to the log file
    :param exc: CustomException object
    :param context: context exception
    :return: Response instance
    """
    serialize = getattr(exc, "serialize", None)
    if isinstance(serialize, ModelSerializer):
        err_handle = ErrorHandler(serialize)
        err_handle.to_log()
        exc.detail = err_handle.response_dict()
        exc.status_code = status.HTTP_400_BAD_REQUEST
    return exception_handler(exc, context)
