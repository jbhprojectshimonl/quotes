from typing import Optional

from quote.models import QuoteLog, Quote


def get_id(pk_from_req: str, name: Optional[str]) -> Optional[int]:
    """
    Returns id of quote by name or by pk
    :param pk_from_req: pk from request object
    :param name: quote name
    :return: id if exist
    """
    filter_dict = dict()
    if pk_from_req is not None:
        filter_dict["id"] = pk_from_req
    elif name is not None:
        filter_dict["name"] = name
    else:
        return
    try:
        return Quote.objects.get(**filter_dict).pk
    except Quote.DoesNotExist:
        return


class DBLogMiddleware:
    """
    Middleware for save any attempt to create/update/delete a quote in DB
    """
    def __init__(self, get_response):
        self.get_response = get_response
        self.pk_from_req = None

    def __call__(self, request):
        response = self.get_response(request)
        if request.method not in ["POST", "PUT", "DELETE"]:
            return response
        data = getattr(response, "data", None)
        quote_log = QuoteLog()
        if data is not None:
            error_code = data.get("errCode")
            if error_code is not None:
                quote_log.error_code = error_code
            description = data.get("description")
            if description:
                quote_log.message = description
        pk = get_id(self.pk_from_req, name=data.get("name") if data else None)
        if pk is not None:
            quote_log.quote_id = pk
        if request.method == "POST":
            quote_log.operation = "C"
        elif request.method == "PUT":
            quote_log.operation = "U"
        elif request.method == "DELETE":
            quote_log.operation = "D"

        quote_log.save()
        return response

    # noinspection PyUnusedLocal
    def process_view(self, request, view_func, view_args, view_kwargs):
        """
        Saves pk from request
        """
        self.pk_from_req = view_kwargs.get("pk")
