from django.core.validators import MinValueValidator
from rest_framework import serializers
from rest_framework.exceptions import ErrorDetail
from rest_framework.fields import empty
from rest_framework.validators import UniqueValidator

from quote.constants import NEGATIVE_INTEGER__ERR_CODE, ALREADY_EXIST__ERR_CODE, EMPTY_NAME__ERR_CODE
from quote.models import Item, Quote


class PricePositiveIntValidator(MinValueValidator):
    """
    Overrides message and code for negative price
    """
    message = "price must be negative integer"
    code = NEGATIVE_INTEGER__ERR_CODE


class NameUniqueValidator(UniqueValidator):
    """
    Overrides message and code for quote already exist
    """
    message = "quote already exist"
    code = ALREADY_EXIST__ERR_CODE


class ItemSerialize(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ["id", "name"]


class QuoteSerialize(serializers.ModelSerializer):
    items = ItemSerialize(many=True, read_only=True)
    name = serializers.CharField(
        max_length=255,
        validators=[NameUniqueValidator(queryset=Quote.objects.all())]
    )
    price = serializers.IntegerField(validators=[PricePositiveIntValidator(0)])

    def __init__(self, instance=None, data=empty, **kwargs):
        super().__init__(instance, data, **kwargs)
        self.custom_errors = dict()  # property for saving custom errors

    def is_valid(self, raise_exception=False):
        valid = super().is_valid(raise_exception)
        name = self.initial_data.get("name")
        if not name or name == "":
            self.custom_errors["name"] = [ErrorDetail("name cannot be empty", code=EMPTY_NAME__ERR_CODE)]
            valid = False
        return valid

    class Meta:
        model = Quote
        fields = ("name", "price", "items")
