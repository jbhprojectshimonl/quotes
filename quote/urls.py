from django.urls import path
from rest_framework_simplejwt import views as jwt_views

from quote.views import QuoteView, QuoteDetail

urlpatterns = [
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('quote/', QuoteView.as_view()),
    path('quote/<int:pk>/', QuoteDetail.as_view())
]