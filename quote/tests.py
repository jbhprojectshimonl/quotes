from django.contrib.auth.models import User
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_200_OK
from rest_framework.test import APITestCase

from quote.models import Item, Quote

QUOTE_NAME = "quote name"

PRICE = 30


class QuoteTestCase(APITestCase):

    def setUp(self):
        User.objects.create_user(username="test", password="test")

        token = self.client.post("/api/token/", data={"username": "test", "password": "test"})
        access_token = token.data.get("access")
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + access_token)

        self.item1 = Item.objects.create(name="item1")
        self.item2 = Item.objects.create(name="item2")
        self.item3 = Item.objects.create(name="item3")
        self.item4 = Item.objects.create(name="item4")

        self.quote = Quote.objects.create(name="quote", price=400)

        self.quote.items.add(self.item1)
        self.quote.items.add(self.item2)
        self.quote.items.add(self.item3)
        self.quote.items.add(self.item4)

        self.quote_pk = self.quote.pk

    def get_valid_data(self):
        name = QUOTE_NAME
        price = PRICE
        items = [dict(id=self.item1.id, name=self.item1.name), dict(id=self.item2.id, name=self.item2.name)]
        return dict(name=name, price=price, items=items)

    def get_exist_quote_name_data(self):
        name = self.quote.name
        price = PRICE
        items = [dict(id=self.item1.id, name=self.item1.name), dict(id=self.item2.id, name=self.item2.name)]
        return dict(name=name, price=price, items=items)

    def get_empty_quote_name_data(self):
        name = ""
        price = PRICE
        items = [dict(id=self.item1.id, name=self.item1.name), dict(id=self.item2.id, name=self.item2.name)]
        return dict(name=name, price=price, items=items)

    def get_empty_item_name_data(self):
        name = QUOTE_NAME
        price = PRICE
        items = [dict(id=self.item1.id, name=""), dict(id=self.item2.id, name=self.item2.name)]
        return dict(name=name, price=price, items=items)

    def get_negative_price_data(self):
        name = QUOTE_NAME
        price = -PRICE
        items = [dict(id=self.item1.id, name=self.item1.name), dict(id=self.item2.id, name=self.item2.name)]
        return dict(name=name, price=price, items=items)

    def test_valid_post(self):
        r = self.client.post("/api/quote/", data=self.get_valid_data(), format='json')
        assert r.status_code == HTTP_201_CREATED, f"{r.data}\n{r.status_code}"

    def test_exist_name_post(self):
        r = self.client.post("/api/quote/", data=self.get_exist_quote_name_data(), format='json')
        assert r.status_code == HTTP_400_BAD_REQUEST, f"{r.data}"

    def test_empty_quote_name_post(self):
        r = self.client.post("/api/quote/", data=self.get_empty_quote_name_data(), format='json')
        assert r.status_code == HTTP_400_BAD_REQUEST, f"{r.data}\n{r.status_code}"

    def test_empty_item_name_post(self):
        r = self.client.post("/api/quote/", data=self.get_empty_item_name_data(), format='json')
        assert r.status_code == HTTP_400_BAD_REQUEST, f"{r.data}\n{r.status_code}"

    def test_negative_price_post(self):
        r = self.client.post("/api/quote/", data=self.get_negative_price_data(), format='json')
        assert r.status_code == HTTP_400_BAD_REQUEST, f"{r.data}\n{r.status_code}"

    def test_valid_put(self):
        r = self.client.put(f"/api/quote/{self.quote_pk}/", data=self.get_valid_data(), format='json')
        assert r.status_code == HTTP_200_OK, f"{r.data}\n{r.status_code}"

    def test_exist_name_put(self):
        r = self.client.put(f"/api/quote/{self.quote_pk}/", data=self.get_exist_quote_name_data(), format='json')
        assert r.status_code == HTTP_200_OK, f"{r.data}"

    def test_empty_quote_name_put(self):
        r = self.client.put(f"/api/quote/{self.quote_pk}/", data=self.get_empty_quote_name_data(), format='json')
        assert r.status_code == HTTP_400_BAD_REQUEST, f"{r.data}\n{r.status_code}"

    def test_empty_item_name_put(self):
        r = self.client.put(f"/api/quote/{self.quote_pk}/", data=self.get_empty_item_name_data(), format='json')
        assert r.status_code == HTTP_400_BAD_REQUEST, f"{r.data}\n{r.status_code}"

    def test_negative_price_put(self):
        r = self.client.put(f"/api/quote/{self.quote_pk}/", data=self.get_negative_price_data(), format='json')
        assert r.status_code == HTTP_400_BAD_REQUEST, f"{r.data}\n{r.status_code}"
