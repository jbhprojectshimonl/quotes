from django.http import Http404
from rest_framework import status
from rest_framework.exceptions import ErrorDetail, APIException
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from quote.constants import ITEM_DOES_NOT_EXIST__ERR_CODE
from quote.models import Quote, Item
from quote.serialize import QuoteSerialize


class CustomException(APIException):

    """
    Extends APIException
    additional serialize property for save serialise object (it's includes errors)
    """
    def __init__(self, serialize=None, detail=None, code=None):
        self.serialize = serialize
        super().__init__(detail, code)


def create_items(quote: Quote, req_items: list, serializer: QuoteSerialize) -> bool:
    """
    Creates associate between quote to items
    :param quote: quote object
    :param req_items: items list from request data
    :param serializer: serializer object for storing errors
    :return: bool (data valid)
    """
    valid = True
    for item in req_items:
        try:
            i = Item.objects.get(id=item["id"], name=item["name"])
            quote.items.add(i)
        except Item.DoesNotExist:
            serializer.custom_errors["items"] = serializer.custom_errors.get("items") or []
            serializer.custom_errors["items"].append(
                ErrorDetail("item does not exist", code=ITEM_DOES_NOT_EXIST__ERR_CODE))
            valid = False
    return valid


class QuoteView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        quotes = Quote.objects.all()
        serializer = QuoteSerialize(quotes, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = QuoteSerialize(data=request.data)
        valid = serializer.is_valid()
        if valid:
            new_quote = serializer.save()
            items_valid = create_items(new_quote, request.data["items"], serializer)
            if items_valid:
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        raise CustomException(serialize=serializer)


class QuoteDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    permission_classes = (IsAuthenticated,)

    def get_object(self, pk):
        try:
            return Quote.objects.get(pk=pk)
        except Quote.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        quote = self.get_object(pk)
        serializer = QuoteSerialize(quote)
        return Response(serializer.data)

    def put(self, request, pk):
        quote = self.get_object(pk)
        serializer = QuoteSerialize(quote, data=request.data)
        if serializer.is_valid():
            serializer.save()
            quote.items.clear()
            items_valid = create_items(quote, request.data["items"], serializer)
            if items_valid:
                return Response(serializer.data, status=status.HTTP_200_OK)
        raise CustomException(serializer)

    def delete(self, request, pk):
        quote = self.get_object(pk)
        quote.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
